package com.example.midterms;


    import android.media.Image;
    import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
    import android.widget.ImageView;
    import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.NumberFormat;

    public class MainActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        }

        public void Eat(View v){
            ImageView hungry = (ImageView) findViewById(R.id.hungrypic);
            hungry.setImageResource(R.drawable.after_cookie);
            TextView quantityTextView = (TextView) findViewById(R.id.hungryText);
            quantityTextView.setText("Im so full");

        }
        public void Reset (View v){
            ImageView hungry = (ImageView) findViewById(R.id.hungrypic);
            hungry.setImageResource(R.drawable.before_cookie);
            TextView quantityTextView = (TextView) findViewById(R.id.hungryText);
            quantityTextView.setText("Im so hungry");


    }
}
